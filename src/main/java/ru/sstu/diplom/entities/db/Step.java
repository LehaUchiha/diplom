package ru.sstu.diplom.entities.db;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by Alex on 06.02.2017.
 */
@DatabaseTable
public class Step {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String name;
    @DatabaseField
    private String information;
    @DatabaseField(columnName = "lesson_id_fk", foreign = true)
    private Lessons lesson;
    @DatabaseField
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getInformation() {
        return information;
    }

    public void setInformation(String information) {
        this.information = information;
    }

    public Lessons getLessons() {
        return lesson;
    }

    public void setLessons(Lessons lesson) {
        this.lesson = lesson;
    }

    @Override
    public String toString() {
        return "Step{" +
                "id=" + id +
                ", name=" + name +
                '}';
    }
}

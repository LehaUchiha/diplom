package ru.sstu.diplom.entities;

/**
 * Created by Alex on 12.02.2017.
 */
public class Statistic {

    private int generation;
    private int population;

    public Statistic(int generation, int population) {
        this.generation = generation;
        this.population = population;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }
}

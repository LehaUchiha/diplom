package ru.sstu.diplom.entities;

/**
 * Created by Alex on 04.12.2016.
 */
public enum FieldConfiguration {
    DEFAULT,
    X30,
    X60,
    X90,
    CUSTOM

}

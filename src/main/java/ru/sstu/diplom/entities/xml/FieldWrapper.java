package ru.sstu.diplom.entities.xml;

import ru.sstu.diplom.entities.model.Cell;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Alex on 29.04.2017.
 */

@XmlRootElement(name = "cells")
public class FieldWrapper {

    private List<List<Cell>> field;

    @XmlElement(name = "cell")
    public List<List<Cell>> getField() {
        return field;
    }

    public void setField(List<List<Cell>> field) {
        this.field = field;
    }
}

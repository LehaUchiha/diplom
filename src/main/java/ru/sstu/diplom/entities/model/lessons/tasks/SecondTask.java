package ru.sstu.diplom.entities.model.lessons.tasks;

import javafx.scene.layout.Pane;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.Task;
import ru.sstu.diplom.parser.impl.JDomParser;
import ru.sstu.diplom.utils.ActionHandlerManager;
import ru.sstu.diplom.utils.CellManager;
import ru.sstu.diplom.utils.GrapchicManager;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Alex on 23.05.2017.
 */
public class SecondTask extends Task {



    public SecondTask() {
        parser = new JDomParser();
        try {
            answer = parser.getField(getClass().getResourceAsStream("/fxml/studypart/lesson2/answer2.xml"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean checkAnswer(List<List<Cell>> field) {

        for(int i=0; i<field.size(); i++){
            for(int j=0; j<field.size(); j++){

                if(answer.get(i).get(j).getState() != field.get(i).get(j).getState())
                    return false;

            }
        }

        return true;
    }

    @Override
    public String getTaskDescription() {
        return "�������: ����� ����� ���� ����������. ������������ ��, �������� ���������� ������ �������. �� �������� ������������ ������ ����������� ������.";
    }

    @Override
    public List<List<Cell>> updateField() throws Exception {

        double size = 405 / 9;
        List<List<Cell>> field = parser.getField(getClass().getResourceAsStream("/fxml/studypart/lesson2/lesson2.xml"));

        for (List<Cell> column: field) {
            for(Cell cell: column)
                cell.resize(size);
        }

        return field;
    }

    @Override
    public String getTeoryInfo() {
        return "�������, ��� ������� � ����������� ������������ ��������� �� ����� �������� ����������������� ���� � ������.";
    }
}

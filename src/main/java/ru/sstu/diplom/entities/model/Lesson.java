package ru.sstu.diplom.entities.model;

import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.lessons.tasks.FirstTask;
import ru.sstu.diplom.entities.model.lessons.tasks.SecondTask;
import ru.sstu.diplom.parser.impl.JDomParser;
import ru.sstu.diplom.utils.ActionHandlerManager;
import ru.sstu.diplom.utils.GrapchicManager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 22.05.2017.
 */
public class Lesson {

    protected List<Task> tasks;

    protected Task currentTask;
    protected JDomParser parser = new JDomParser();

    public boolean checkAnswer(List<List<Cell>> field){
        return currentTask.checkAnswer(field);
    }

    public void nextTask(VBox toolsPane, Label topToolsBar, Pane centerBar, Label commentInfo) throws Exception {
        int index = tasks.indexOf(currentTask);
        if(tasks.size() > index+1){
            currentTask = tasks.get(index+1);
        }
        if(tasks.size() == index+1){
            Alert aler = new Alert(Alert.AlertType.INFORMATION);
            aler.setTitle("������������!");
            aler.setContentText("��� ���������� ������� ������ ���������");
            aler.setHeaderText("�� ������� ��������� ��� �������");
            aler.showAndWait();
        }

    }

    public String getTaskDescription(){
        return currentTask.getTaskDescription();
    }

    public String getTeoryInfo(){
        return currentTask.getTeoryInfo();

    }

    public List<List<Cell>> updateField() throws Exception {
        return currentTask.updateField();
    }
}

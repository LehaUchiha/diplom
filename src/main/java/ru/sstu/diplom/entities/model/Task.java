package ru.sstu.diplom.entities.model;

import javafx.scene.layout.Pane;
import ru.sstu.diplom.parser.impl.JDomParser;
import ru.sstu.diplom.utils.CellManager;
import ru.sstu.diplom.utils.GrapchicManager;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Alex on 23.05.2017.
 */
public abstract class Task {

    protected List<List<Cell>> answer;

    protected JDomParser parser;

//    public static final int FIRST_TASK = 0;
//    public static final int SECOND_TASK = 1;
//    public static final int THIRD_TASK = 3;



    public abstract  boolean checkAnswer(List<List<Cell>> field);

    public abstract String getTaskDescription();

    public abstract List<List<Cell>> updateField() throws Exception;

    public abstract String getTeoryInfo();

}

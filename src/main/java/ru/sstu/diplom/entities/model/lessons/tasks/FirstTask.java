package ru.sstu.diplom.entities.model.lessons.tasks;

import javafx.scene.layout.Pane;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.Task;
import ru.sstu.diplom.parser.impl.JDomParser;
import ru.sstu.diplom.utils.CellManager;
import ru.sstu.diplom.utils.GrapchicManager;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Alex on 23.05.2017.
 */
public class FirstTask extends Task {

    @Override
    public boolean checkAnswer(List<List<Cell>> field) {
        boolean flag1 = false;
        boolean flag2 = false;

        for(List<Cell> column: field){
            for (Cell cell: column){
                if(cell.getState() == CellStates.T001 || cell.getState() == CellStates.T011 || cell.getState() == CellStates.T021 || cell.getState() == CellStates.T031)
                    flag1 = true;
                if(cell.getState() == CellStates.T100 || cell.getState() == CellStates.T110 || cell.getState() == CellStates.T120 || cell.getState() == CellStates.T130)
                    flag2 = true;
            }
        }

        return flag1 && flag2;
    }

    @Override
    public String getTaskDescription() {
        return "�������:  ��������� �� ���� ������ � ������� � ����������� ������������ ����������. ������� ������������ ��������� �������� ��� ����� �������, � ����������� ��� �������. ����������� �� ����� ��������. ������������ ������ ����� � ������� ����������� � ������� ����������� �����������:";
    }

    @Override
    public List<List<Cell>> updateField() {
        double dh = 405;
        double size = dh / 9;
        List<List<Cell>> field = CellManager.getFields(9, 9, size);
        return field;

    }

    @Override
    public String getTeoryInfo() {
        return "������������ ��������� ������������� ��� �������� �������� � ������� ����������� ������� ������� ������������ ��������� �������� ����� ������. ����������� ������������ ��������� �������� �������.";
    }
}

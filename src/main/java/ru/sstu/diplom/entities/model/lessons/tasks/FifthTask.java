package ru.sstu.diplom.entities.model.lessons.tasks;

import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.Task;
import ru.sstu.diplom.parser.impl.JDomParser;

import java.io.File;
import java.util.List;

/**
 * Created by Alex on 23.05.2017.
 */
public class FifthTask extends Task {

    public FifthTask() {
        parser = new JDomParser();
    }

    @Override
    public boolean checkAnswer(List<List<Cell>> field) {
        CellStates leftState = field.get(3).get(3).getState();
        CellStates topState = field.get(4).get(2).getState();
        CellStates rightState = field.get(5).get(3).getState();
        if((leftState == CellStates.T010 || leftState == CellStates.T020 || leftState == CellStates.T030 || leftState == CellStates.T110 || leftState == CellStates.T120 || leftState == CellStates.T130) &&
                (topState == CellStates.T010 || topState == CellStates.T020 || topState == CellStates.T000 || topState == CellStates.T110 || topState == CellStates.T120 || topState == CellStates.T100) &&
                (rightState == CellStates.T010 || rightState == CellStates.T030 || rightState == CellStates.T000 || rightState == CellStates.T110 || rightState == CellStates.T130 || rightState == CellStates.T100) )
            return true;
        return false;
    }

    @Override
    public String getTaskDescription() {
        return "�������: ����������� ����� ������������ ��������� �����, ������ � ������ �� ������������� ���������. ������� ��������� �� ������ ���� ���������� �� ������������ ���������. � ���������� �������� 3 ��������.";
    }

    @Override
    public List<List<Cell>> updateField() throws Exception {
        double size = 405 / 9;
        List<List<Cell>> field = parser.getField(getClass().getResourceAsStream("/fxml/studypart/lesson3/lesson2.xml"));

        for (List<Cell> column: field) {
            for(Cell cell: column)
                cell.resize(size);
        }

        return field;
    }

    @Override
    public String getTeoryInfo() {

        return "������������ ��������� ��������� ��������� ������ �� ���������. ���� � ����� �� 4� ������ �������� �������, �� ������������ ��������� �������� �������� ��� � 3 ������ ������� ��� ������� ������������ ���������.";
    }
}

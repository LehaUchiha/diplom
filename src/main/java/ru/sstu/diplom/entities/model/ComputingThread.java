package ru.sstu.diplom.entities.model;

import javafx.application.Platform;
import ru.sstu.diplom.entities.Statistic;
import ru.sstu.diplom.utils.ActionHandlerManager;
import ru.sstu.diplom.utils.GrapchicManager;
import ru.sstu.diplom.utils.Utils;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Alex on 10.02.2017.
 */
public class ComputingThread extends Thread {

    private ComputingCore computingCore;
    private volatile boolean isRunning = true;
    private long delay = 500;

    public ComputingThread(ComputingCore computingCore){
        this.computingCore = computingCore;
    }

    @Override
    public void run() {
        while (isRunning) {

            Platform.runLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        List<List<Cell>> field = atomicOperation();
                        GrapchicManager.update(field);
                        ActionHandlerManager.handleField(field);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                }});
            try {
                Thread.sleep(delay);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private List<List<Cell>> atomicOperation(){
        return computingCore.compute();
    }

    private List<List<Cell>> getFirstStep(){
        return computingCore.compute();
    }

    public void stopComputing(){
        isRunning = false;

    }

    public void stepComputing(){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                List<List<Cell>> field = atomicOperation();
                try {
                    GrapchicManager.update(field);
                    ActionHandlerManager.handleField(field);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }});

    }

    public void setDelay(long delay) {
        this.delay = delay;
    }


    public void goToGeneration(int generation) {
        try {
            List<List<Cell>> field = null;
            for (int i=0; i<generation; i++) {
                field = atomicOperation();
            }
            if(generation == 0)
                field = computingCore.getFirstStep();
            GrapchicManager.update(field);
            ActionHandlerManager.handleField(field);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


}

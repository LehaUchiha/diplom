package ru.sstu.diplom.entities.model.lessons.tasks;

import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.Task;
import ru.sstu.diplom.parser.impl.JDomParser;

import java.io.File;
import java.util.List;

/**
 * Created by Alex on 23.05.2017.
 */
public class ThirdTask extends Task {

    public ThirdTask() {
        parser = new JDomParser();
    }

    @Override
    public boolean checkAnswer(List<List<Cell>> field) {
        if(field.get(3).get(2).getState() == CellStates.T131 || field.get(3).get(4).getState() == CellStates.T111 ||
                field.get(2).get(3).getState() == CellStates.T101 || field.get(4).get(3).getState() == CellStates.T121)
            return true;
        return false;
    }

    @Override
    public String getTaskDescription() {
        return "�������: ���������� ���������� ������� ������������ ���������. ��� ����� ��������� ������������ ������������ ������������ ��������� ������� � �������� ������������� ���������.";
    }

    @Override
    public List<List<Cell>> updateField() throws Exception {

        double size = 405 / 9;
        List<List<Cell>> field = parser.getField(getClass().getResourceAsStream("/fxml/studypart/lesson2/lesson3.xml"));

        for (List<Cell> column: field) {
            for(Cell cell: column)
                cell.resize(size);
        }

        return field;
    }

    @Override
    public String getTeoryInfo() {
        return "������ ���������� ����������� ������������ ��������� - ����������� ������� ������������ � ������������ ���������.";
    }
}

package ru.sstu.diplom.entities.model.lessons;

import ru.sstu.diplom.entities.model.Lesson;
import ru.sstu.diplom.entities.model.lessons.tasks.*;

import java.util.ArrayList;

/**
 * Created by Alex on 23.05.2017.
 */
public class SecondLesson extends Lesson {

    public SecondLesson() {
        tasks = new ArrayList<>();
        tasks.add(new ForthTask());
        tasks.add(new FifthTask());
        tasks.add(new SixTask());
        currentTask = tasks.get(0);
    }
}

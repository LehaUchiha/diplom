package ru.sstu.diplom.entities.model;

import javafx.application.Platform;
import javafx.scene.control.TableView;
import ru.sstu.diplom.constants.ApplicationOperation;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.constants.ComputingCoreCommand;
import ru.sstu.diplom.controllers.AbstractController;
import ru.sstu.diplom.controllers.ApplicationController;
import ru.sstu.diplom.entities.Statistic;
import ru.sstu.diplom.utils.*;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.util.*;

import static ru.sstu.diplom.constants.CellStates.*;

/**
 * Created by Alex on 04.12.2016.
 */
public class ComputingCore {

    private AbstractController controller;
    private ComputingThread computingThread = null;
    private TableView iterationTable;
    private volatile int generation = 0;
    private volatile int poppulation = 0;
    private AdditionInformationHandler infoHandler;
    private List<CellStates> notActiveStates;
    private List<CellStates> activeStates;
    private long delay = 500;
    private Map<Integer, List<List<Cell>>> memory;
    private List<List<Cell>> firstStep;

    private List<List<Cell>> field;


    public ComputingCore(List<List<Cell>> field, AbstractController controller) {
        memory = new HashMap<>();
        this.field = field;
        infoHandler = new AdditionInformationHandler();
        notActiveStates = Arrays.asList(T000, T010, T020, T030, T100, T110, T120, T130, U, SO, S0, S1, S00, S01, S10, S11, S000, C00);
        activeStates = Arrays.asList(T001, T011, T021, T031);
        this.controller = controller;
    }

    public void setField(List<List<Cell>> field){
        this.field = field;
    }

    public void execute(ComputingCoreCommand command) {
        if(ComputingCoreCommand.START.equals(command)) {
            if(computingThread == null) {
                computingThread = new ComputingThread(this);
                computingThread.setDelay(delay);
                firstStep = field;
                computingThread.start();
            }
        }
        if(ComputingCoreCommand.STOP.equals(command)) {
            if(computingThread != null) {
                computingThread.stopComputing();
                computingThread = null;
                generation = 0;
                firstStep = null;
                infoHandler.clearTable(iterationTable);
                controller.setField(field);
            }
            ActionHandlerManager.clearField(field);
            memory.clear();

        }
        if(ComputingCoreCommand.PAUSE.equals(command)) {
            if(computingThread != null) {
                computingThread.stopComputing();
                computingThread = null;
            }

        }
    }

    public void executeGeneration(int dxGen) {
        if(computingThread == null)
            computingThread = new ComputingThread(this);
        computingThread.setDelay(0);
        computingThread.goToGeneration(dxGen);
        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(generation, Utils.countPopulation(field)));
        computingThread = null;

        memory.put(generation, field);
        if(generation == 0)
            firstStep = field;
    }

    public void executeGenerationBack(int dxGen) {
        field = firstStep;
        if(computingThread == null)
            computingThread = new ComputingThread(this);
        computingThread.setDelay(0);
        computingThread.goToGeneration(dxGen);
        generation = dxGen;
        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(generation, Utils.countPopulation(field)));
        computingThread = null;

        memory.put(generation, field);
        if(generation == 0)
            firstStep = field;
    }

    public void stepBack() throws FileNotFoundException {
        if(generation > 0) {
            generation -= 1;
            if(memory.containsKey(generation)) {
                field = memory.get(generation);
                memory.remove(generation);
            }
            GrapchicManager.update(field);
            ActionHandlerManager.handleField(field);
            infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(generation, Utils.countPopulation(field)));
        }

    }

    public List<List<Cell>> compute() {

        if(generation == 0)
            firstStep = field;

        if (generation >= 0) {
            if(memory.size() == 50) {
                memory.put(generation, field);
                memory.remove(generation-50);
            }
            else
                memory.put(generation, field);
        }

        ++generation;

        Cell[][] resultArray = convertListToArray(field);

        for (int i = 0; i < field.size(); i++) {
            for (int j = 0; j < field.size(); j++) {
                Cell cell = new Cell(field.get(i).get(j));

                switch (cell.getState()) {
                    case T000: {

                        /*
                         *T000 - текущее состояние клетки.
                         *Если слева, справа, сверху или снизу от нее находятся клетки с обычным
                         *активным транзитивным состоянием с направлением стрелки в сторону текущей, то присвоить
                         * текущей клетки статус T001
                         */
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = T001;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        /*
                         *T000 - текущее состояние клетки.
                         *Если слева, справа, сверху или снизу от нее находятся клетки со специальным
                         *активным транзитивным состоянием с направлением стрелки в сторону текущей, то необходимо
                         * уничтожить текущую клетку присвоив статус U
                         */
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 || j > 0 &&
                                field.get(i).get(j - 1).getState() == T131 || j < field.size() - 1 &&
                                field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        /*
                         *T000 - текущее состояние клетки.
                         *Если слева, справа, сверху или снизу от нее находятся клетки с конфлюэнтным состоянием,
                         * активным на данный момент, то необходимо
                         * уничтожить текущую клетку, присвоив статус U
                         */
                        if (i > 0 && (field.get(i - 1).get(j).getState() == C10 ||
                                field.get(i - 1).get(j).getState() == C11) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 ||
                                        field.get(i).get(j + 1).getState() == C11) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == C10 ||
                                        field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = T001;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        //                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 || j > 0 &&
//                                field.get(i).get(j - 1).getState() == T131 || j < field.size() - 1 &&
//                                field.get(i).get(j + 1).getState() == T111) {
//                            CellStates state = U;
//                            cell.setState(state);
//                            resultArray[i][j] = cell;
//                        }
                        break;
                    }

                    case T010: {
                        if (j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = T011;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11) ||
                                i > 0 && field.get(i - 1).get(j).getState() == C10 ||
                                i > 0 && field.get(i - 1).get(j).getState() == C11 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C10 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C11) {
                            CellStates state = T011;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }



                        break;
                    }

                    case T020: {
                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = T021;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (i < field.size() - 1 && (field.get(i + 1).get(j).getState() == C10 || field.get(i + 1).get(j).getState() == C11) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = (T021);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T030: {
                        if (j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = (T031);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11) ||
                                i > 0 && field.get(i - 1).get(j).getState() == C10 ||
                                i > 0 && field.get(i - 1).get(j).getState() == C11 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C10 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C11) {
                            CellStates state = (T031);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case T001: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T000);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j > 0 && notActiveStates.contains(field.get(i).get(j - 1).getState())) {
                            CellStates state = (T000);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T000);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (T001);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && (field.get(i - 1).get(j).getState() == C10 || field.get(i - 1).get(j).getState() == C11)) {
                            CellStates state = (T001);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T011: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T010);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T010);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T010);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                j < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (T011);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                j < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11)) {
                            CellStates state = (T011);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T021: {
                        if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T020);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j > 0 && notActiveStates.contains(field.get(i).get(j - 1).getState())) {
                            CellStates state = (T020);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T020);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (T021);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && (field.get(i + 1).get(j).getState() == C10 || field.get(i + 1).get(j).getState() == C11)) {
                            CellStates state = (T021);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case T031: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T030);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T030);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T030);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = (T031);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = (T031);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    //Специальные состояния

                    case T100: {
                         /*
                         *T100 - текущее состояние клетки.
                         *Если слева, сверху или снизу от нее находятся клетки со специальным
                         *активным транзитивным состоянием с направлением стрелки в сторону текущей, то
                         * присвоить текущей клетке активное состояние T101
                         */
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = (T101);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        /*
                         *T100 - текущее состояние клетки.
                         *Если слева, сверху или снизу от нее находятся клетки с обычным
                         *активным транзитивным состоянием с направлением стрелки в сторону текущей, то
                         *текущая клетка уничтожается с присвоением статуса U
                         */
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 || j > 0 &&
                                field.get(i).get(j - 1).getState() == T031 || j < field.size() &&
                                field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }


                        if (i > 0 && (field.get(i - 1).get(j).getState() == C10 || field.get(i - 1).get(j).getState() == C11)) {
                            CellStates state = (T101);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                         /*
                         *T100 - текущее состояние клетки.
                         *Если слева, сверху или снизу от нее находятся клетки с конфлюэнтным
                         *активным на данный момент состоянием, то
                         * присвоить текущей клетке активное состояние T101
                         */
                        if (i > 0 && (field.get(i - 1).get(j).getState() == C10 || field.get(i - 1).get(j).getState() == C11) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = T101;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T110: {
                        if (j < field.size() && field.get(i).get(j + 1).getState() == T111 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = (T111);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j < field.size() && field.get(i).get(j + 1).getState() == T011 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11) ||
                                i > 0 && field.get(i - 1).get(j).getState() == C10 ||
                                i > 0 && field.get(i - 1).get(j).getState() == C11 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C10 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C11) {
                            CellStates state = (T111);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T120: {
                        if (i < field.size() && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = (T121);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (i < field.size() && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (i < field.size() - 1 && (field.get(i + 1).get(j).getState() == C10 || field.get(i + 1).get(j).getState() == C11) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = (T121);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T130: {
                        if (j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = (T131);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11) ||
                                i > 0 && field.get(i - 1).get(j).getState() == C10 ||
                                i > 0 && field.get(i - 1).get(j).getState() == C11 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C10 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == C11) {
                            CellStates state = (T131);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case T101: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T100);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j > 0 && notActiveStates.contains(field.get(i).get(j - 1).getState())) {
                            CellStates state = (T100);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T100);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = (T101);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && (field.get(i - 1).get(j).getState() == C10 || field.get(i - 1).get(j).getState() == C11)) {
                            CellStates state = (T101);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T111: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T110);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T110);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T110);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                j < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = (T111);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                j < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j < field.size() - 1 && (field.get(i).get(j + 1).getState() == C10 || field.get(i).get(j + 1).getState() == C11)) {
                            CellStates state = (T111);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case T121: {
                        if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T120);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j > 0 && notActiveStates.contains(field.get(i).get(j - 1).getState())) {
                            CellStates state = (T120);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() - 1 && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T120);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = (T121);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && (field.get(i + 1).get(j).getState() == C10 || field.get(i + 1).get(j).getState() == C11)) {
                            CellStates state = (T121);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case T131: {
                        if (i > 0 && notActiveStates.contains(field.get(i - 1).get(j).getState())) {
                            CellStates state = (T130);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (j < field.size() && notActiveStates.contains(field.get(i).get(j + 1).getState())) {
                            CellStates state = (T130);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i < field.size() - 1 && notActiveStates.contains(field.get(i + 1).get(j).getState())) {
                            CellStates state = (T130);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T121) {
                            CellStates state = (T131);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() && field.get(i + 1).get(j).getState() == T021) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        if (j > 0 && (field.get(i).get(j - 1).getState() == C10 || field.get(i).get(j - 1).getState() == C11)) {
                            CellStates state = (T131);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case U: {
                        /**
                         * Текущая клетка имеет базовое состояние U
                         * Если текущая клетка получает импульс от обычного или специального транзитивных состояний
                         * с любой из сторон окрестности фон Неймана, то ее состояние меняется на сэнсетивное S                         *
                         */

                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101)) {
                            CellStates state = (SO);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121)) {
                            CellStates state = (SO);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131)) {
                            CellStates state = (SO);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (SO);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        break;
                    }

                    case SO: {
                        /*
                         *Текущее клетка имеет счувственное состояние S
                         *Если клетка в следующий момент времени получает иипульс с какой-лиюо
                         * стороны, тои меняе состояние на S1
                         * Если же клетка импульс не получпет, то меняет сво
                         * состояние на S0
                         */
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (S1);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else {
                            CellStates state = (S0);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S0: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (S01);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (S00);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S1: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (S11);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (S10);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S00: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (T020);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (S000);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S01: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (T100);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (T030);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S10: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (T120);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (T110);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S11: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (C00);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (T130);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case S000: {
                        if (i > 0 && (field.get(i - 1).get(j).getState() == T001 || field.get(i - 1).get(j).getState() == T101) ||
                                i < field.size() - 1 && (field.get(i + 1).get(j).getState() == T021 || field.get(i + 1).get(j).getState() == T121) ||
                                j > 0 && (field.get(i).get(j - 1).getState() == T031 || field.get(i).get(j - 1).getState() == T131) ||
                                j < field.size() - 1 && (field.get(i).get(j + 1).getState() == T011 || field.get(i).get(j + 1).getState() == T111)) {
                            CellStates state = (T010);
                            cell.setState(state);
                            resultArray[i][j] = cell;

                        } else {
                            CellStates state = (T000);
                            cell.setState(state);
                        }
                        resultArray[i][j] = cell;

                        break;
                    }

                    case C00: {

                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (C01);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case C01: {
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (C11);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i > 0 && field.get(i - 1).get(j).getState() != T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() != T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() != T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() != T011) {
                            CellStates state = (C10);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case C10: {
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (C01);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i > 0 && field.get(i - 1).get(j).getState() != T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() != T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() != T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() != T011) {
                            CellStates state = (C00);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    case C11: {
                        if (i > 0 && field.get(i - 1).get(j).getState() == T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T011) {
                            CellStates state = (C11);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        } else if (i > 0 && field.get(i - 1).get(j).getState() != T001 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() != T021 ||
                                j > 0 && field.get(i).get(j - 1).getState() != T031 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() != T011) {
                            CellStates state = (C10);
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }
                        if (i > 0 && field.get(i - 1).get(j).getState() == T101 ||
                                i < field.size() - 1 && field.get(i + 1).get(j).getState() == T121 ||
                                j > 0 && field.get(i).get(j - 1).getState() == T131 ||
                                j < field.size() - 1 && field.get(i).get(j + 1).getState() == T111) {
                            CellStates state = U;
                            cell.setState(state);
                            resultArray[i][j] = cell;
                        }

                        break;
                    }

                    default: {
                        resultArray[i][j] = cell;
                        break;
                    }
                }
            }
        }
        field = convertArrayToList(resultArray);

        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(generation, Utils.countPopulation(field)));

        return field;
    }

    public void setGenerationTable(TableView iterationTable){
        this.iterationTable = iterationTable;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }

    private Cell [][] convertListToArray(List<List<Cell>> field){
        Cell [][] arrayField = new Cell [field.size()][field.size()];
        int i = 0;
        int j = 0;
        for(List<Cell> row: field){
            for(Cell cell: row){
                arrayField[i][j] = new Cell(cell);
                ++j;
            }
            ++i;
            j=0;
        }
        return arrayField;
    }

    private List<List<Cell>>  convertArrayToList(Cell [][] field) {
        List<List<Cell>> listField = new ArrayList<List<Cell>>();

        for(int i=0; i<field.length; i++){
            List<Cell> list = new ArrayList();
            for(int j=0; j<field.length; j++){
                list.add(field[i][j]);
            }
            listField.add(list);
        }

        return listField;
    }

    public void setDelay(long delay){
        this.delay = delay;
        if(computingThread != null)
            computingThread.setDelay(delay);
    }

    public int getGeneration(){
        return generation;
    }

    public List<List<Cell>> getFirstStep() {
        return firstStep;
    }
}

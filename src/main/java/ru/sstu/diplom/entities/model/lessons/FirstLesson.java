package ru.sstu.diplom.entities.model.lessons;

import ru.sstu.diplom.entities.model.Lesson;
import ru.sstu.diplom.entities.model.lessons.tasks.FirstTask;
import ru.sstu.diplom.entities.model.lessons.tasks.SecondTask;
import ru.sstu.diplom.entities.model.lessons.tasks.ThirdTask;

import java.util.ArrayList;

/**
 * Created by Alex on 23.05.2017.
 */
public class FirstLesson extends Lesson {

    public FirstLesson() {
        tasks = new ArrayList<>();
        tasks.add(new FirstTask());
        tasks.add(new SecondTask());
        tasks.add(new ThirdTask());
        currentTask = tasks.get(0);
    }
}

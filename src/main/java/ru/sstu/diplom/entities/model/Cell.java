package ru.sstu.diplom.entities.model;

import javafx.application.Platform;
import javafx.scene.shape.Rectangle;
import ru.sstu.diplom.constants.CellStates;

import static ru.sstu.diplom.constants.CellStates.*;


/**
 * Created by Alex on 20.11.2016.
 */

public class Cell extends Rectangle {

    private CellStates state;
    private  double DEFAULT_SIZE = 25;
    private int coordX;
    private int coordY;
    private String isActive;
    private String isNotActive;
    private String notSupported;

    public  Cell(int coordX, int coordY){
        this.coordX = coordX;
        this.coordY = coordY;
        isActive = "�������";
        isNotActive = "���������";
        notSupported = "�� �����";
        setX(DEFAULT_SIZE * coordX);
        setY(DEFAULT_SIZE * coordY);
        setWidth(DEFAULT_SIZE);
        setHeight(DEFAULT_SIZE);
        state = CellStates.U;
    }

    public Cell(Cell cell){
        coordX = cell.getCoordX();
        coordY = cell.getCoordY();
        isActive = "�������";
        isNotActive = "���������";
        notSupported = "�� �����";
        setX(cell.getSize() * coordX);
        setY(cell.getSize() * coordY);
        setWidth(cell.getSize());
        setHeight(cell.getSize());
        state = cell.getState();

//        if(state.isActive())
//            active = "yes";
//        else
//            active = "no";
    }

    public CellStates getState(){
        return state;
    }

    public void setState(CellStates state) {
        this.state = state;
    }

    public double getSize() {
        return getHeight();
    }

    public void setSize(double size) {
        setWidth(size);
        setHeight(size);
    }

    public void resize(double size){
        setX(size * coordX);
        setY(size * coordY);
        setSize(size);
    }

    public int getCoordX() {
        return coordX;
    }

    public void setCoordX(int coordX) {
        this.coordX = coordX;
    }

    public int getCoordY() {
        return coordY;
    }

    public void setCoordY(int coordY) {
        this.coordY = coordY;
    }

    public boolean isActive(){
        return state.isActive();
    }

    public void setActive(boolean flag){
        if(flag)
            switch(state){
                case T000:{ state = T001; break;}
                case T010:{ state = T011; break;}
                case T020:{ state = T021; break;}
                case T030:{ state = T031; break;}
                case T100:{ state = T101; break;}
                case T110:{ state = T111; break;}
                case T120:{ state = T121; break;}
                case T130:{ state = T131; break;}
            }
        else
            switch (state){
                case T001:{ state = T000; break;}
                case T011:{ state = T010; break;}
                case T021:{ state = T020; break;}
                case T031:{ state = T030; break;}
                case T101:{ state = T100; break;}
                case T111:{ state = T110; break;}
                case T121:{ state = T120; break;}
                case T131:{ state = T130; break;}
            }
    }

    public String getIsActive() {
        return isActive;
    }

    public String getIsNotActive() {
        return isNotActive;
    }

    public String getNotSupported() {
        return notSupported;
    }
}

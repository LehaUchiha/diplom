package ru.sstu.diplom.ui;

import javafx.scene.Node;
import javafx.scene.control.Label;
import ru.sstu.diplom.entities.db.Lessons;

/**
 * Created by Alex on 10.02.2017.
 */
public class LabelExtended extends Label {

    private Lessons hideValue;

    public LabelExtended() {
        super();
    }

    public LabelExtended(String text) {
        super(text);
    }

    public LabelExtended(String text, Node graphic) {
        super(text, graphic);

    }

    public Lessons getHideValue() {
        return hideValue;
    }

    public void setHideValue(Lessons hideValue) {
        this.hideValue = hideValue;
    }
}

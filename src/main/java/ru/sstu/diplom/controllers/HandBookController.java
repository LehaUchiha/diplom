package ru.sstu.diplom.controllers;

import javafx.animation.FadeTransition;
import javafx.event.*;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.util.Duration;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.utils.builders.TreeViewBuilder;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Alex on 20.05.2017.
 */


public class HandBookController implements Initializable {

    private TreeViewBuilder treeViewBuilder;

    @FXML
    Pane content;
    @FXML
    TabPane information;
    @FXML
    AnchorPane parentHandBook;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        treeViewBuilder = new TreeViewBuilder(information);
        TreeView tree = treeViewBuilder.createTreeView();
        tree.setMaxHeight(content.getMaxHeight());
        content.getChildren().add(tree);

        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1.5), parentHandBook);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(100.0);
        fadeTransition.play();
    }

}

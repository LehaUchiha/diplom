package ru.sstu.diplom.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import ru.sstu.diplom.utils.detailedData.DetailedDataProcessor;
import ru.sstu.diplom.utils.detailedData.impl.BaseDetailedDataImpl;
import ru.sstu.diplom.utils.detailedData.impl.TransitiveDetailedDataImpl;
import ru.sstu.diplom.utils.impl.DetailedDataFabric;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Created by Alex on 06.05.2017.
 */
public class DetailedInformationController implements Initializable {

    @FXML
    GridPane infoTable;

    @FXML
    Button btnCancel;

    private String state;

    private DetailedDataFabric detailedDataFabric;
    private DetailedDataProcessor dataProcessor;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        detailedDataFabric = new DetailedDataFabric();

        dataProcessor =  detailedDataFabric.getDataAbout(state);


        if("U".equals(state.substring(0,1))){
            infoTable.add(dataProcessor.getState(), 1, 0);
            infoTable.add(dataProcessor.getImage(), 1, 1);
            infoTable.add(dataProcessor.getFormula(), 1, 2);
            infoTable.add(dataProcessor.getInformation(), 1, 3);
        }
        if("T".equals(state.substring(0,1)) || "C".equals(state.substring(0,1))){
            infoTable.add(dataProcessor.getState(), 1, 0);
            infoTable.add(dataProcessor.getStatus(), 1, 1);
            infoTable.add(dataProcessor.getImage(), 1, 2);
            infoTable.add(dataProcessor.getFormula(), 1, 3);
            infoTable.add(dataProcessor.getEncryption(), 1, 4);
            infoTable.add(dataProcessor.getDescription(), 1, 5);
            infoTable.add(dataProcessor.getInformation(), 1, 6);
        }
        if("S".equals(state.substring(0,1))){
            infoTable.add(dataProcessor.getState(), 1, 0);
            infoTable.add(dataProcessor.getImage(), 1, 1);
            infoTable.add(dataProcessor.getFormula(), 1, 2);
            infoTable.add(dataProcessor.getDescription(), 1, 3);
            infoTable.add(dataProcessor.getTreeImage(), 1, 4);
        }

    }

    public DetailedInformationController(String state) {
        this.state = state;
    }

    public void cancelTransitionPopulation(ActionEvent actionEvent) {
        Stage stage = (Stage)btnCancel.getScene().getWindow();
        stage.close();
    }



}

package ru.sstu.diplom.controllers;

import javafx.animation.FadeTransition;
import javafx.application.Platform;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.effect.Lighting;

import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import ru.sstu.diplom.Main;
import ru.sstu.diplom.constants.ApplicationOperation;
import ru.sstu.diplom.constants.ComputingCoreCommand;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.Statistic;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.ComputingCore;

import ru.sstu.diplom.parser.Parser;
import ru.sstu.diplom.parser.impl.JDomParser;

import ru.sstu.diplom.utils.*;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Created by Alex on 16.10.2016.
 */
public class ApplicationController extends AbstractController {

    private ApplicationController instance;
    private ComputingCore computingCore;
    private ApplicationOperation operation = ApplicationOperation.CURSOR;
    private List<List<Cell>> field;
    AdditionInformationHandler infoHandler;
    private Parser parser;
    private int FIELD_SIZE = 25;

    @FXML VBox parentApplication;
    @FXML
    private Pane paneContainer,additionInfoPanel;
    @FXML
    private ToolBar modellingModeToolbar;
    @FXML
    private TableView tableCell,iterationTable;
    @FXML
    private VBox baseInfoPanel,toolsPane,transitivePane,confluentPane,sensetivePane,unstatePane;
    @FXML
    private Label sliderValue;
    @FXML
    private Slider delaySlider;
    @FXML
    private Button btnPlay, btnPause,btnStop,btnStepBack,btnStep,btnGeneration, btnGenerationback;
    @FXML
    private TextField generationCount;
    @FXML
    private Menu menuConfig;
    @FXML
    private MenuItem saveMenuItem, loadMenuItem, firstLesson, secondLesson;

    @FXML
    public void initialize(URL location, ResourceBundle resources){
        instance = this;
        initField(FIELD_SIZE, FIELD_SIZE);
        try {
            actionHandler();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        setToolTipsForButton();
        infoHandler = new AdditionInformationHandler();
        computingCore = new ComputingCore(field, this);
        computingCore.setGenerationTable(iterationTable);

        sliderValue.setText(Utils.aroundTo(delaySlider.getValue()));

        delaySlider.valueProperty().addListener(new ChangeListener<Number>() {
            public void changed(ObservableValue<? extends Number> obsVal,
                                Number oldVal, Number newVal) {
                sliderValue.setText(newVal.intValue()+"");
                computingCore.setDelay(newVal.intValue());
            }
        });

        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1.5), parentApplication);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(100.0);
        fadeTransition.play();
    }

    private void setToolTipsForButton(){
        btnPlay.setTooltip(new Tooltip("������"));
        btnPause.setTooltip(new Tooltip("�����"));
        btnStop.setTooltip(new Tooltip("����"));
        btnStep.setTooltip(new Tooltip("��� ������"));
        btnStepBack.setTooltip(new Tooltip("��� �����"));
        btnGeneration.setTooltip(new Tooltip("������� �� ����� ���������"));
    }

    public void exit(ActionEvent actionEvent) {
        System.exit(0);
    }

    private void initField(int width, int height){

        paneContainer.getChildren().clear();
        field = CellManager.getFields(width, height);
        for (List<Cell> column: field)
            paneContainer.getChildren().addAll(column);
        ActionHandlerManager.tableView = tableCell;
        GrapchicManager.applicationController = this;
    }

    private void initField(int width, int height, double size){

        paneContainer.getChildren().clear();
        field = CellManager.getFields(width, height, size);

        for (List<Cell> column: field)
            paneContainer.getChildren().addAll(column);
        ActionHandlerManager.tableView = tableCell;
        GrapchicManager.applicationController = this;
    }

    private void actionHandler() throws FileNotFoundException {
        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
        GrapchicManager.update(field);
    }

    public void play(ActionEvent actionEvent) {
        computingCore.execute(ComputingCoreCommand.START);
        setDisable(true);
    }

    public void pause(ActionEvent actionEvent) {
        computingCore.execute(ComputingCoreCommand.PAUSE);
        setDisable(false);
    }

    private void setDisable(boolean flag){
        btnStep.setDisable(flag);
        btnGeneration.setDisable(flag);
        btnStepBack.setDisable(flag);
        menuConfig.setDisable(flag);
        saveMenuItem.setDisable(flag);
        loadMenuItem.setDisable(flag);
        firstLesson.setDisable(flag);
        secondLesson.setDisable(flag);
        btnGenerationback.setDisable(flag);
    }

    public void stop(ActionEvent actionEvent) {
        computingCore.execute(ComputingCoreCommand.STOP);
        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(0, 0));
        setDisable(false);
        try {
            actionHandler();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        computingCore.setGeneration(0);
    }

    public void stepForward(ActionEvent actionEvent) {
        computingCore.executeGeneration(1);
    }


    public void setCellActive(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.SET_ACTIVE;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setCursor(MouseEvent mouseEvent) throws FileNotFoundException {

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = ApplicationOperation.CURSOR;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    ActionHandlerManager.handleField(field);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }});
    }

    public void setT00(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T00;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;

        ActionHandlerManager.handleField(field);
        GrapchicManager.update(field);
    }

    public void setT01(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T01;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT02(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T02;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT03(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T03;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT10(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T10;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT11(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T11;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT12(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T12;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT13(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T13;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setC(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.C;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setSO(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.SO;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS0(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S0;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS1(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S1;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS00(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S00;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS01(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S01;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS10(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S10;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS11(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S11;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS000(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S000;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setU(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.U;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void clearEffectsFromToolPanel(){
        for(Node node: toolsPane.getChildren())
            node.setEffect(null);

        for(Node node: transitivePane.getChildren())
            node.setEffect(null);

        for(Node node: confluentPane.getChildren())
            node.setEffect(null);

        for(Node node: sensetivePane.getChildren())
            node.setEffect(null);

        for(Node node: unstatePane.getChildren())
            node.setEffect(null);

    }

    public void updateField(List<List<Cell>> field){
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                paneContainer.getChildren().clear();
                for (List<Cell> column : field)
                    paneContainer.getChildren().addAll(column);
            }});
    }

    public List<List<Cell>> getField() {
        return field;
    }

    public void setField(List<List<Cell>> field) {
        this.field = field;
    }

    @Override
    public void backToMenu(ActionEvent actionEvent) {
        computingCore.execute(ComputingCoreCommand.STOP);
        super.backToMenu(actionEvent);

    }

    public void stepBack(ActionEvent actionEvent) {
        try {
            computingCore.stepBack();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void goToGeneration(ActionEvent actionEvent) {
        int generation = getStepShift();
        if(generation != 0)
            computingCore.executeGeneration(generation);
    }


    private int getStepShift(){
        TextInputDialog dialog = new TextInputDialog("0");
        dialog.setTitle("�������");
        dialog.setTitle("�������");
        dialog.setHeaderText("���������� �� �����\n ���������:");
        dialog.initModality(Modality.APPLICATION_MODAL);

        dialog.getEditor().addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            public void handle( KeyEvent t ) {
                char ar[] = t.getCharacter().toCharArray();
                char ch = ar[t.getCharacter().toCharArray().length - 1];
                if (!(ch >= '0' && ch <= '9')) {
                    t.consume();
                }
            }
        });

        Optional<String> result = dialog.showAndWait();
        int generation = 0;
        if(result.isPresent()) {
            try {
                generation = Integer.parseInt(result.get());
            } catch (NumberFormatException e) {
                Alert error = new Alert(Alert.AlertType.ERROR, "�������� ���� ������ �����");
                error.setTitle("������");
                error.setHeaderText("������");
                error.showAndWait();
            }
        }
        return generation;
    }

    public void goToGenerationBack(ActionEvent actionEvent){
        int generation = getStepShift();
        int dx = computingCore.getGeneration()-generation;
        if(generation !=0 && dx >= 0)

            computingCore.executeGenerationBack(dx);

    }

    public void loadConfiguration(ActionEvent actionEvent) {
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML files (*.xml)", "*.xml");
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(extFilter);
        parser = new JDomParser();

        File file = fileChooser.showOpenDialog(STAGE);

        if(file != null) {
            try {
                field = parser.getField(new FileInputStream(file));
                paneContainer.getChildren().clear();
                for (List<Cell> column : field)
                    paneContainer.getChildren().addAll(column);
                ActionHandlerManager.tableView = tableCell;
                actionHandler();
                computingCore = new ComputingCore(field, this);
                computingCore.setGenerationTable(iterationTable);

            } catch (Exception e) { // catches ANY exception
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("������");
                alert.setHeaderText("�������� ������������ �����: " +file.getName());
                alert.setContentText("����: "+file.getAbsolutePath());

                alert.showAndWait();
            }
        }
    }

    public void saveConfiguration(ActionEvent actionEvent) {

        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter(
                "XML Files (*.xml)", "*.xml");
        FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().add(extFilter);
        parser = new JDomParser();
        File file = fileChooser.showSaveDialog(STAGE);
        if(file != null)
            parser.saveObject(file, field);
    }

    public void setFieldSizeX30(ActionEvent actionEvent) throws FileNotFoundException {
        FIELD_SIZE = 30;
        double dh = STAGE.getScene().heightProperty().get() - 75;
        double size = dh / FIELD_SIZE;
        initField(FIELD_SIZE, FIELD_SIZE, size);
        actionHandler();
        computingCore = new ComputingCore(field, this);
        computingCore.setGenerationTable(iterationTable);
        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(0, 0));
    }

    public void setFieldSizeX45(ActionEvent actionEvent) throws FileNotFoundException {
        FIELD_SIZE = 45;
        double dh = STAGE.getScene().heightProperty().get() -75;
        double size = dh / FIELD_SIZE;
        initField(FIELD_SIZE, FIELD_SIZE, size);
        actionHandler();
        computingCore = new ComputingCore(field, this);
        computingCore.setGenerationTable(iterationTable);
        infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(0, 0));
    }

    public void setFieldCustom(ActionEvent actionEvent) {
        TextInputDialog dialog = new TextInputDialog("0");
        dialog.setTitle("������ ����");
        dialog.setHeaderText("������� ���������� ����� � ��������");
        dialog.initModality(Modality.APPLICATION_MODAL);

        dialog.getEditor().addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
            public void handle( KeyEvent t ) {
                char ar[] = t.getCharacter().toCharArray();
                char ch = ar[t.getCharacter().toCharArray().length - 1];
                if (!(ch >= '0' && ch <= '9')) {
                    t.consume();
                }
            }
        });

        Optional<String> result = dialog.showAndWait();
        int sizeField = 0;
        if(result.isPresent()) {
            try {
                sizeField = Integer.parseInt(result.get());

                if(sizeField >= 45)
                    FIELD_SIZE = 45;
                else if(sizeField <=3)
                    FIELD_SIZE = 3;
                else FIELD_SIZE = sizeField;

                double dh = STAGE.getScene().heightProperty().get()-75;
                double size = dh / FIELD_SIZE;
                initField(FIELD_SIZE, FIELD_SIZE, size);
                actionHandler();
                computingCore = new ComputingCore(field, this);
                computingCore.setGenerationTable(iterationTable);
                infoHandler.showInformationAboutGenerationAndPopulation(iterationTable, new Statistic(0, 0));
            } catch (NumberFormatException e) {
                Alert error = new Alert(Alert.AlertType.ERROR, "�������� ���� ������ �����");
                error.setTitle("������");
                error.setHeaderText("������");
                error.showAndWait();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }


    }

    public void goToHandBook(ActionEvent actionEvent) throws IOException {
        Stage stage = new Stage();
        FXMLLoader loader = new FXMLLoader();

        Parent root = loader.load(getClass().getResourceAsStream(ViewConstants.HANDBOOK_FXML));

        Scene scene = new Scene(root);
        STAGE.setResizable(false);
        Main.acceptStyle(scene);
        stage.setScene(scene);
        stage.setTitle("����������");
        stage.show();
    }

    public void goToFirstLesson(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setController(new LessonController(LessonManager.FIRST_LESSON));
        Parent root = loader.load(getClass().getResourceAsStream(ViewConstants.LESSON_FXML));

        Scene scene = new Scene(root);
        LessonController controller = loader.getController();
        controller.addScreenSizeListener(controller.getFieldContainer());
        Main.acceptStyle(scene);
        STAGE.setScene(scene);
        STAGE.setMaxWidth(1150);
        STAGE.setTitle("���� 1");
    }

    public void goToSecondLesson(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setController(new LessonController(LessonManager.SECOND_LESSON));
        Parent root = loader.load(getClass().getResourceAsStream(ViewConstants.LESSON2_FXML));

        Scene scene = new Scene(root);

        Main.acceptStyle(scene);
        STAGE.setScene(scene);
        STAGE.setMaxWidth(1110);
        STAGE.setTitle("���� 2");
    }

    public void addScreenSizeListener(Node root){

        //root.getScene().
        root.getScene().heightProperty().addListener(
                new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        double size = (newValue.doubleValue() - 75) / FIELD_SIZE;

                        for(List<Cell> column: field){
                            for(Cell cell: column)
                                cell.resize(size);
                        }
                        try {
                            actionHandler();
                            computingCore.setField(field);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );

//        root.getScene().widthProperty().addListener(
//                new ChangeListener<Number>() {
//                    @Override
//                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
//                        System.out.println("width: "+newValue);
//                    }
//                }
//        );
    }

    public Node getFieldContainer(){
        return paneContainer;
    }
}

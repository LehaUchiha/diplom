package ru.sstu.diplom.controllers;

import javafx.animation.FadeTransition;
import javafx.animation.RotateTransition;
import javafx.animation.TranslateTransition;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Lighting;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.util.Duration;
import ru.sstu.diplom.Main;
import ru.sstu.diplom.constants.ApplicationOperation;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.db.Lessons;
import ru.sstu.diplom.entities.db.Step;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.ComputingCore;
import ru.sstu.diplom.entities.model.Lesson;
import ru.sstu.diplom.services.StepService;
import ru.sstu.diplom.utils.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.net.URL;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Alex on 06.02.2017.
 */
public class LessonController extends AbstractController implements Initializable {

    @FXML
    private Pane centerBar, rightToolBar;
    @FXML
    private VBox toolPanel;
    @FXML
    private Label commentInfo, taskInfo;
    @FXML
    private GridPane topBarPanel1;
    @FXML
    private AnchorPane lessonParent;
    private int FIELD_SIZE = 9;

    @FXML Label T000;
    @FXML Label activeCell;
    private int task = 1;
    private Lesson lesson;
    private LessonManager lessonManager;

    @FXML
    private VBox baseInfoPanel,toolsPane,transitivePane,confluentPane,sensetivePane,unstatePane;

    private List<List<Cell>> field;
    private ApplicationOperation operation = ApplicationOperation.CURSOR;

    public LessonController(int lessonNum) {
        lessonManager = new LessonManager(lessonNum);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        try {
            double dh = STAGE.getScene().heightProperty().get() - 50;
            System.out.println(dh);
            double size = dh / 9;
            field = lessonManager.getLesson().updateField();
            initField(field);
            actionHandler();

            FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1.5), lessonParent);
            fadeTransition.setFromValue(0.0);
            fadeTransition.setToValue(1.0);
            fadeTransition.play();

            RotateTransition rotateTransition = new RotateTransition(Duration.seconds(5), activeCell);
            rotateTransition.setFromAngle(0);
            rotateTransition.setToAngle(360);

            rotateTransition.setCycleCount(50);
            rotateTransition.play();


        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initField(List<List<Cell>> field) throws FileNotFoundException {
        centerBar.getChildren().clear();
        for (List<Cell> column: field)
            centerBar.getChildren().addAll(column);
        GrapchicManager.applicationController = this;

    }

    private void initField(int width, int height, double size) throws FileNotFoundException {

        centerBar.getChildren().clear();
        field = CellManager.getFields(width, height, size);
        for (List<Cell> column: field)
            centerBar.getChildren().addAll(column);
        GrapchicManager.applicationController = this;

    }

    public void setCursor(MouseEvent mouseEvent) throws FileNotFoundException {

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = ApplicationOperation.CURSOR;
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    ActionHandlerManager.handleField(field);
                    //  GrapchicManager.update(field);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }});
    }

    public void clearEffectsFromToolPanel(){
        for(Node node: toolPanel.getChildren())
            node.setEffect(null);
    }

    public void setCellActive(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.SET_ACTIVE;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT00(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T00;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;

        ActionHandlerManager.handleField(field);
        GrapchicManager.update(field);
    }

    public void setT01(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T01;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT02(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T02;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT03(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T03;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT10(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T10;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT11(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T11;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT12(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T12;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setT13(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.T13;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setC(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.C;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setSO(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.SO;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS0(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S0;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS1(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S1;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS00(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S00;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS01(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S01;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS10(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S10;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS11(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S11;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setS000(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.S000;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    public void setU(MouseEvent mouseEvent) throws FileNotFoundException {
        operation = ApplicationOperation.U;

        clearEffectsFromToolPanel();
        Label label = (Label) mouseEvent.getSource();
        label.setEffect(new Lighting());

        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
    }

    private void actionHandler() throws FileNotFoundException {
        ActionHandlerManager.OPERATION = operation;
        ActionHandlerManager.handleField(field);
        GrapchicManager.update(field);
    }

    public void endStudy(ActionEvent actionEvent) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setController(new ApplicationController());

        Parent root = loader.load(getClass().getResourceAsStream(ViewConstants.APPLICATION_FXML));
        Scene scene = new Scene(root);

        ApplicationController controller = loader.getController();
        controller.addScreenSizeListener(controller.getFieldContainer());
        Main.acceptStyle(scene);
        STAGE.setScene(scene);
        STAGE.setMaxWidth(1350);
        STAGE.setTitle("��������� ���������� �������� ��� �������");
    }

    public void cleanField(ActionEvent actionEvent) throws Exception {
        taskInfo.setText(lessonManager.getLesson().getTaskDescription());
        commentInfo.setText(lessonManager.getLesson().getTeoryInfo());
        field = lessonManager.getLesson().updateField();
        centerBar.getChildren().clear();
        for(List<Cell> column: field)
            centerBar.getChildren().addAll(column);
        GrapchicManager.update(field);
        ActionHandlerManager.handleField(field);
    }

    public void checkAnswer(ActionEvent actionEvent) throws Exception {

        if(lessonManager.checkAnswer(field)){
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "���������� �����");
            alert.setTitle("����������");
            alert.setHeaderText("���������");
            alert.showAndWait();

            lessonManager.nextTask(lessonParent, toolsPane, taskInfo, centerBar, commentInfo);

            taskInfo.setText(lessonManager.getLesson().getTaskDescription());
            commentInfo.setText(lessonManager.getLesson().getTeoryInfo());
            field = lessonManager.getLesson().updateField();
            centerBar.getChildren().clear();
            for(List<Cell> column: field)
                centerBar.getChildren().addAll(column);
            GrapchicManager.update(field);
            ActionHandlerManager.handleField(field);

        }else {
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "��������� �����! ����������� �������.");
            alert.setTitle("����������");
            alert.setHeaderText("���������");
            alert.showAndWait();
        }
    }

    public void addScreenSizeListener(Node root) {

        root.getScene().heightProperty().addListener(
                new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        System.out.println("height " + newValue);
                        double size = (newValue.doubleValue() - 185) / FIELD_SIZE;
                        System.out.println(newValue);

                        for (List<Cell> column : field) {
                            for (Cell cell : column)
                                cell.resize(size);
                        }
                        try {
                            actionHandler();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
        root.getScene().widthProperty().addListener(
                new ChangeListener<Number>() {
                    @Override
                    public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                        System.out.println(newValue);
                    }
                });
    }

    public Node getFieldContainer(){
        return centerBar;
    }
}

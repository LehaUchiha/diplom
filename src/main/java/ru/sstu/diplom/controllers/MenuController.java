package ru.sstu.diplom.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import org.apache.log4j.Logger;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.db.Lessons;
import ru.sstu.diplom.services.LessonService;
import ru.sstu.diplom.ui.LabelExtended;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Alex on 22.11.2016.
 */
public class MenuController extends AbstractController implements Initializable {

   // private static final Logger LOGGER = LoggerFactory.getLogger(MenuController.class);

    @FXML
    private VBox contentPanel;
    @FXML
    private VBox descriptionPanel;
    @FXML
    private VBox itemPanel;
    @FXML
    private Label constructorLbl;
    @FXML
    private Text textLbl;
    @FXML
    private Button goToLessonBtn;


    private LessonService lessonService;

    @FXML
    public void initialize(URL location, ResourceBundle resources) {

//        if(LOGGER.isDebugEnabled())
//            LOGGER.debug("initialize started");

        try {
            lessonService = new LessonService();
            initContent();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void executeApplication(MouseEvent mouseEvent) throws IOException {

        if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
            if(mouseEvent.getClickCount() == 1) {
                textLbl.setText("Конструктор предоставляет позможность создать свою конфигурацию клеточного автомата и проследить за его работой");
                goToLessonBtn.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        try {
                            changeScene(ViewConstants.APPLICATION_FXML);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
            }
            if(mouseEvent.getClickCount() == 2) {
//                if (LOGGER.isDebugEnabled())
//                    LOGGER.debug("Moved to Application.fxml");

                changeScene(ViewConstants.APPLICATION_FXML);
            }
        }
    }

    private void initContent() throws SQLException {

        List<LabelExtended> labels = new ArrayList<LabelExtended>();
        for(final Lessons lesson: lessonService.getAll()){

            LabelExtended label = new LabelExtended(lesson.getName());
            label.setHideValue(lesson);
            label.setFont(Font.font(14));

            label.setOnMouseClicked(new javafx.event.EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {

                    textLbl.setText(lesson.getDescription());
                    me.consume();
                }
            });

            labels.add(label);
            System.out.println(lesson.getName());
        }

        addEffectToLabel(labels);

        itemPanel.getChildren().addAll(labels);
    }

    private void addEffectToLabel(List<LabelExtended> labels){

        constructorLbl.setOnMouseExited(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                constructorLbl.setEffect(null);
            }
        });

        constructorLbl.setOnMouseEntered(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent me) {
                constructorLbl.setEffect(new DropShadow(5, Color.CHOCOLATE));
            }
        });

        for(final LabelExtended label: labels){
            label.setPadding(new Insets(0, 10, 0, 20));

            label.setOnMouseExited(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    label.setEffect(null);
                }
            });

            label.setOnMouseEntered(new EventHandler<MouseEvent>() {
                public void handle(MouseEvent me) {
                    label.setEffect(new DropShadow(5, Color.CHOCOLATE));
                }
            });

            label.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if(mouseEvent.getButton().equals(MouseButton.PRIMARY)){
                        if(mouseEvent.getClickCount() == 1){
                            textLbl.setText(label.getHideValue().getDescription());
                            goToLessonBtn.setOnAction(new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                 //   LessonController.setLesson(label.getHideValue());
                                    try {
                                        changeScene(ViewConstants.LESSON_FXML);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });
                        }
                        if(mouseEvent.getClickCount() == 2){
                         //   LessonController.setLesson(label.getHideValue());

                            try {
                                changeScene(ViewConstants.LESSON_FXML);
                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });
        }
    }


}

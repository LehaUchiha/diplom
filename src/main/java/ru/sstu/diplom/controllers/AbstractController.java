package ru.sstu.diplom.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ru.sstu.diplom.Main;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.model.Cell;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by Alex on 27.09.2016.
 */
public class AbstractController implements Initializable {

    public static Stage STAGE;

    protected void changeScene(String view) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        Parent root = (Parent) loader.load(getClass().getResourceAsStream(view));
        Scene scene = new Scene(root);
        Main.acceptStyle(scene);
        STAGE.setScene(scene);
        STAGE.show();
    }

    public void backToMenu(ActionEvent actionEvent) {
        try {
            changeScene(ViewConstants.MENU_FXML);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setField(List<List<Cell>> field){}

    public void updateField(List<List<Cell>> field){}


    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
}

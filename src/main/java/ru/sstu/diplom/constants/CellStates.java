package ru.sstu.diplom.constants;

/**
 * Created by Alex on 11.02.2017.
 */
public enum CellStates {
    T000, T001, T010, T011, T020, T021, T030, T031, T100, T101, T110, T111, T120, T121, T130, T131,
    C00, C10, C01, C11, U, SO, S0, S1, S00, S01, S10, S11, S000;

    public CellStates getState(String state){
        if(this.name().equals(state)){
            return this;
        }
        return this;
    }

    public boolean isSupportActive(){
        String name = this.name();
        if("U".equals(name.substring(name.length()-1, name.length())) || "S".equals(name.substring(0, 1)))
                return false;
        return true;
    }

    public boolean isActive(){
        String name = this.name();
        if("C".equals(name.substring(0, 1))){
            if("1".equals(name.substring(1, 2)))
                return true;
            else
                return false;
        }
        if("1".equals(name.substring(name.length()-1, name.length())))
            return true;
        return false;
    }
}

package ru.sstu.diplom.constants;

/**
 * Created by Alex on 16.10.2016.
 */
public class ViewConstants {
    public static final String APPLICATION_FXML = "/fxml/Application.fxml";
    public static final String MENU_FXML = "/fxml/Menu.fxml";
    public static final String LESSON_FXML = "/fxml/Lesson.fxml";
    public static final String LESSON2_FXML = "/fxml/Lesson2.fxml";
    public static final String DT000 = "/fxml/modal/DetailedT000.fxml";
    public static final String DT010 = "/fxml/modal/DetailedT010.fxml";
    public static final String DT020 = "/fxml/modal/DetailedT020.fxml";
    public static final String DT030 = "/fxml/modal/DetailedT030.fxml";
    public static final String DT001 = "/fxml/modal/DetailedT001.fxml";
    public static final String DT011 = "/fxml/modal/DetailedT011.fxml";
    public static final String DT021 = "/fxml/modal/DetailedT021.fxml";
    public static final String DT031 = "/fxml/modal/DetailedT031.fxml";
    public static final String DT100 = "/fxml/modal/DetailedT100.fxml";
    public static final String DT110 = "/fxml/modal/DetailedT110.fxml";
    public static final String DT120 = "/fxml/modal/DetailedT120.fxml";
    public static final String DT130 = "/fxml/modal/DetailedT130.fxml";
    public static final String DT101 = "/fxml/modal/DetailedT101.fxml";
    public static final String DT111 = "/fxml/modal/DetailedT111.fxml";
    public static final String DT121 = "/fxml/modal/DetailedT121.fxml";
    public static final String DT131 = "/fxml/modal/DetailedT131.fxml";
    public static final String DC00 = "/fxml/modal/DetailedC00.fxml";
    public static final String DC01 = "/fxml/modal/DetailedC01.fxml";
    public static final String DC10 = "/fxml/modal/DetailedC10.fxml";
    public static final String DC11 = "/fxml/modal/DetailedC11.fxml";
    public static final String DSQ = "/fxml/modal/DetailedSQ.fxml";
    public static final String DS0 = "/fxml/modal/DetailedS0.fxml";
    public static final String DS1 = "/fxml/modal/DetailedS1.fxml";
    public static final String DS00 = "/fxml/modal/DetailedS00.fxml";
    public static final String DS01 = "/fxml/modal/DetailedS01.fxml";
    public static final String DS10 = "/fxml/modal/DetailedS10.fxml";
    public static final String DS11 = "/fxml/modal/DetailedS11.fxml";
    public static final String DS000 = "/fxml/modal/DetailedS11.fxml";
    public static final String DU = "/fxml/modal/DetailedU.fxml";
    public static final String HANDBOOK_FXML = "/fxml/studypart/HandBook.fxml";
}

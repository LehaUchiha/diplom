package ru.sstu.diplom.constants;

/**
 * Created by Alex on 10.02.2017.
 */
public enum ComputingCoreCommand {
    START, PAUSE, STOP, STEP, GENERATION
}

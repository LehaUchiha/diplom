package ru.sstu.diplom.constants;

/**
 * Created by Alex on 10.02.2017.
 */
public enum ApplicationOperation {
    CURSOR, SET_ACTIVE, T00, T01, T02, T03, T10, T11, T12, T13,
    C, U, SO, S0, S1, S00, S01, S10, S11, S000;
}

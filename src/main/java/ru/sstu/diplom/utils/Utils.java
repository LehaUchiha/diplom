package ru.sstu.diplom.utils;

import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;


import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by Alex on 27.02.2017.
 */
public class Utils {

    public static String aroundTo(double value){
        String formatedDouble = String.format("%.10f", value);
        DecimalFormat df = new DecimalFormat("#");
        return df.format(value);
    }

    public static int countPopulation(List<List<Cell>> field){
        int count = 0;
        for(List<Cell> row: field){
            for(Cell cell: row){
                if(cell.getState() != CellStates.U)
                    count++;
            }
        }
        return count;
    }

}

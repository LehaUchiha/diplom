package ru.sstu.diplom.utils.builders;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.controllers.DetailedInformationController;

import java.io.IOException;

/**
 * Created by Alex on 20.05.2017.
 */
public class TreeViewBuilder {

    private TabPane information;

    public TreeViewBuilder(TabPane information) {
        this.information = information;
    }

    public TreeView createTreeView(){

        Label handbookLabel = new Label("����������");
        Label transitiveLabel = new Label("������������ ���������");
        Label confluentLabel = new Label("������������ ���������");
        Label sensitiveLabel = new Label("����������� ���������");
        Label ULabel = new Label("������� ��������� U");
        Label T000Label = new Label("��������� T000");
        Label T010Label = new Label("��������� T010");
        Label T020Label = new Label("��������� T020");
        Label T030Label = new Label("��������� T030");
        Label T001Label = new Label("��������� T001");
        Label T011Label = new Label("��������� T011");
        Label T021Label = new Label("��������� T021");
        Label T031Label = new Label("��������� T031");
        Label T100Label = new Label("��������� T100");
        Label T110Label = new Label("��������� T110");
        Label T120Label = new Label("��������� T120");
        Label T130Label = new Label("��������� T130");
        Label T101Label = new Label("��������� T101");
        Label T111Label = new Label("��������� T111");
        Label T121Label = new Label("��������� T121");
        Label T131Label = new Label("��������� T131");
        Label C00Label = new Label("��������� C00");
        Label C01Label = new Label("��������� C01");
        Label C10Label = new Label("��������� C10");
        Label C11Label = new Label("��������� C11");
        Label SQLabel = new Label("��������� S");
        Label S0Label = new Label("��������� S0");
        Label S1Label = new Label("��������� S1");
        Label S00Label = new Label("��������� S00");
        Label S01Label = new Label("��������� S01");
        Label S10Label = new Label("��������� S10");
        Label S11Label = new Label("��������� S11");
        Label S000Label = new Label("��������� S000");
        //Label transitionTreeLabel = new Label("������ ���������");
        addAction(ULabel, ViewConstants.DU, "U");
        addAction(T000Label, ViewConstants.DT000, "T000");
        addAction(T010Label, ViewConstants.DT000, "T010");
        addAction(T020Label, ViewConstants.DT000, "T020");
        addAction(T030Label, ViewConstants.DT000, "T030");
        addAction(T001Label, ViewConstants.DT000, "T001");
        addAction(T011Label, ViewConstants.DT000, "T011");
        addAction(T021Label, ViewConstants.DT000, "T021");
        addAction(T031Label, ViewConstants.DT000, "T031");
        addAction(T100Label, ViewConstants.DT000, "T100");
        addAction(T110Label, ViewConstants.DT000, "T110");
        addAction(T120Label, ViewConstants.DT000, "T120");
        addAction(T130Label, ViewConstants.DT000, "T130");
        addAction(T101Label, ViewConstants.DT000, "T101");
        addAction(T111Label, ViewConstants.DT000, "T111");
        addAction(T121Label, ViewConstants.DT000, "T121");
        addAction(T131Label, ViewConstants.DT000, "T131");
        addAction(C00Label, ViewConstants.DT000, "C00");
        addAction(C01Label, ViewConstants.DT000, "C01");
        addAction(C10Label, ViewConstants.DT000, "C10");
        addAction(C11Label, ViewConstants.DT000, "C11");
        addAction(SQLabel, ViewConstants.DSQ, "SO");
        addAction(S0Label, ViewConstants.DSQ, "S0");
        addAction(S1Label, ViewConstants.DSQ, "S1");
        addAction(S01Label, ViewConstants.DSQ, "S01");
        addAction(S10Label, ViewConstants.DSQ, "S10");
        addAction(S11Label, ViewConstants.DSQ, "S11");
        addAction(S00Label, ViewConstants.DSQ, "S00");
        addAction(S000Label, ViewConstants.DSQ, "S000");

        TreeItem<Label> root = new TreeItem<>(handbookLabel);
        root.setExpanded(true);

        addBranch(ULabel, root, false);
        TreeItem<Label> transitiveItem = addBranch(transitiveLabel, root, true);
        TreeItem<Label> confluentItem = addBranch(confluentLabel, root, true);
        TreeItem<Label> sensitiveItem = addBranch(sensitiveLabel, root, true);
        addBranch(ULabel, root, false);
        addBranch(T000Label, transitiveItem, false);
        addBranch(T010Label, transitiveItem, false);
        addBranch(T020Label, transitiveItem, false);
        addBranch(T030Label, transitiveItem, false);
        addBranch(T001Label, transitiveItem, false);
        addBranch(T011Label, transitiveItem, false);
        addBranch(T021Label, transitiveItem, false);
        addBranch(T031Label, transitiveItem, false);
        addBranch(T100Label, transitiveItem, false);
        addBranch(T110Label, transitiveItem, false);
        addBranch(T120Label, transitiveItem, false);
        addBranch(T130Label, transitiveItem, false);
        addBranch(T101Label, transitiveItem, false);
        addBranch(T111Label, transitiveItem, false);
        addBranch(T121Label, transitiveItem, false);
        addBranch(T131Label, transitiveItem, false);
        addBranch(C00Label, confluentItem, false);
        addBranch(C01Label, confluentItem, false);
        addBranch(C10Label, confluentItem, false);
        addBranch(C11Label, confluentItem, false);
        addBranch(SQLabel, sensitiveItem, false);
        addBranch(S0Label, sensitiveItem, false);
        addBranch(S1Label, sensitiveItem, false);
        addBranch(S00Label, sensitiveItem, false);
        addBranch(S01Label, sensitiveItem, false);
        addBranch(S10Label, sensitiveItem, false);
        addBranch(S11Label, sensitiveItem, false);
        addBranch(S000Label, sensitiveItem, false);
       // addBranch(transitionTreeLabel, root, false);

        return new TreeView(root);
    }

    protected TreeItem<Label> addBranch(Label name, TreeItem parent, boolean isExpand){
        TreeItem<Label> branch = new TreeItem(name);
        branch.setExpanded(isExpand);
        parent.getChildren().add(branch);
        return branch;
    }

    protected void addAction(Label label, String view, String state){
        label.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    FXMLLoader loader = new FXMLLoader();
                    loader.setController(new DetailedInformationController(state));
                    Parent root = loader.load(getClass().getResourceAsStream(view));
                    Tab tab = new Tab(label.getText(), root);
                    tab.setClosable(true);

                    information.setTabClosingPolicy(TabPane.TabClosingPolicy.SELECTED_TAB);
                    information.getTabs().add(tab);
                    information.getTabs().get(0);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

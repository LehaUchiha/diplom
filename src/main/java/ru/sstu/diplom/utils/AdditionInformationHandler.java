package ru.sstu.diplom.utils;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.Statistic;
import ru.sstu.diplom.entities.model.Cell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 03.12.2016.
 */
public class AdditionInformationHandler {

    private List<TableView> tableViews = new ArrayList<TableView>();

    private TableColumn state;
    private TableColumn active;

    private TableColumn generation;
    private TableColumn population;

    public void showInformationInTable(TableView table, Cell cell){

        table.setItems(null);

        state = (TableColumn) table.getColumns().get(0);
        state.setCellValueFactory(new PropertyValueFactory<Cell, CellStates>("state"));

        active = (TableColumn) table.getColumns().get(1);


        if(cell.getState().isSupportActive()) {
            boolean activeState = cell.getState().isActive();
            if (activeState)
                active.setCellValueFactory(new PropertyValueFactory<Cell, String>("isActive"));
            else
                active.setCellValueFactory(new PropertyValueFactory<Cell, String>("isNotActive"));
        }
        else
            active.setCellValueFactory(new PropertyValueFactory<Cell, String>("notSupported"));

        ObservableList<Cell> data = FXCollections.observableArrayList(cell);

        table.setItems(data);
    }

    public void showInformationAboutGenerationAndPopulation(TableView table, Statistic statistic){
        generation = (TableColumn) table.getColumns().get(0);
        generation.setCellValueFactory(new PropertyValueFactory<Statistic, String>("generation"));

        population = (TableColumn) table.getColumns().get(1);
        population.setCellValueFactory(new PropertyValueFactory<Statistic, String>("population"));

        ObservableList<Statistic> data = FXCollections.observableArrayList(statistic);

        table.setItems(data);
    }

    public void clearTable(TableView table){
        table.setItems(null);
    }
}

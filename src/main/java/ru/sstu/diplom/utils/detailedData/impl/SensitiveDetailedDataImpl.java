package ru.sstu.diplom.utils.detailedData.impl;

import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.TranslateTransition;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;
import javafx.util.Duration;
import org.jdom.JDOMException;
import ru.sstu.diplom.parser.impl.DetailedDataXmlParser;
import ru.sstu.diplom.utils.detailedData.DetailedDataProcessor;

import java.io.IOException;

/**
 * Created by Alex on 21.05.2017.
 */
public class SensitiveDetailedDataImpl extends DetailedDataProcessor {

    public SensitiveDetailedDataImpl(String state) throws JDOMException, IOException {
        dataParser = new DetailedDataXmlParser();
        this.element = dataParser.parseState(state);
    }

    @Override
    public Pane getTreeImage() {
        Pane p = new Pane();
        p.setId(element.getChild("tree").getText());

        p.setOnMouseClicked(new EventHandler<MouseEvent>() {
            private boolean flag = false;

            double from_x_scale = p.getScaleX();
            double from_y_scale = p.getScaleY();
            double to_x_scale = p.getScaleX()+p.getScaleX()*0.6;
            double to_y_scale = p.getScaleY()+p.getScaleY()*0.6;

            double from_x_coord = p.getLayoutX();
            double from_y_coord = p.getLayoutY();
            double to_x_coord = p.getLayoutX() - 20;
            double to_y_coord = p.getLayoutY() - 130;
            ScaleTransition scaleTransition = new ScaleTransition(Duration.seconds(1), p);
            TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(1), p);
            ParallelTransition parallelTransition = new ParallelTransition(p, scaleTransition, translateTransition);
            @Override
            public void handle(MouseEvent event) {

                if(!flag) {
                    translateTransition.setFromX(from_x_coord);
                    translateTransition.setFromY(from_y_coord);
                    translateTransition.setToX(to_x_coord);
                    translateTransition.setToY(to_y_coord);

                    scaleTransition.setFromX(from_x_scale);
                    scaleTransition.setFromY(from_y_scale);
                    scaleTransition.setToX(to_x_scale);
                    scaleTransition.setToY(to_y_scale);
                    parallelTransition.play();

                flag = true;
                }else{
                    translateTransition.setFromX(to_x_coord);
                    translateTransition.setFromY(to_y_coord);
                    translateTransition.setToX(from_x_coord);
                    translateTransition.setToY(from_y_coord);

                    scaleTransition.setFromX(to_x_scale);
                    scaleTransition.setFromY(to_y_scale);
                    scaleTransition.setToX(from_x_scale);
                    scaleTransition.setToY(from_y_scale);
                    parallelTransition.play();

                    flag = false;
                }
            }
        });
        return p;
    }

    @Override
    public Label getDescription() {
        Label label = new Label(element.getChild("description").getText());
        label.setWrapText(true);
        return label;
    }
}

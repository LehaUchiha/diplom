package ru.sstu.diplom.utils.detailedData;


import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import org.jdom.Element;
import ru.sstu.diplom.parser.impl.DetailedDataXmlParser;

/**
 * Created by Alex on 20.05.2017.
 */
public abstract class DetailedDataProcessor {
    protected Element element;

    protected DetailedDataXmlParser dataParser;

    public DetailedDataProcessor() {
        dataXmlParser = new DetailedDataXmlParser();
    }

    protected DetailedDataXmlParser dataXmlParser;

    public Label getState() {
        return new Label(element.getChild("state").getValue());
    }

    public Pane getImage() {
        Pane p = new Pane();
        p.setId(element.getChild("img").getText());
        return p;
    }

    public Label getFormula() {
        return new Label(element.getChild("formula").getText());
    }

    public Label getInformation() {
        Label label = new Label(element.getChild("information").getText());
        label.setWrapText(true);
        return label;
    }

    public Label getStatus() {
        throw new UnsupportedOperationException();
    }

    public Label getDescription() {
        throw new UnsupportedOperationException();
    }

    public Label getEncryption(){
        throw new UnsupportedOperationException();
    }

    public Pane getTreeImage() {
        throw new UnsupportedOperationException();
    }

}

package ru.sstu.diplom.utils.detailedData.impl;

import javafx.beans.property.Property;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import ru.sstu.diplom.parser.impl.DetailedDataXmlParser;
import ru.sstu.diplom.utils.detailedData.DetailedDataProcessor;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by Alex on 20.05.2017.
 */
public class TransitiveDetailedDataImpl extends DetailedDataProcessor {

    public TransitiveDetailedDataImpl(String state) throws Exception {
        dataParser = new DetailedDataXmlParser();
        this.element = dataParser.parseState(state);
    }



    @Override
    public Label getStatus() {
        return new Label(element.getChild("status").getText());
    }


    @Override
    public Label getDescription() {
        Label label = new Label(element.getChild("description").getText());
        label.setWrapText(true);
        return label;
    }

    @Override
    public Label getEncryption(){
        Label label = new Label(element.getChild("decrypt").getText());
        label.setWrapText(true);
        return label;
    }



}

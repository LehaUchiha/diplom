package ru.sstu.diplom.utils.detailedData.impl;

import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import org.jdom.JDOMException;
import ru.sstu.diplom.parser.impl.DetailedDataXmlParser;
import ru.sstu.diplom.utils.detailedData.DetailedDataProcessor;

import java.io.IOException;

/**
 * Created by Alex on 21.05.2017.
 */
public class BaseDetailedDataImpl extends DetailedDataProcessor {

    public BaseDetailedDataImpl(String state) throws JDOMException, IOException {
        dataParser = new DetailedDataXmlParser();
        this.element = dataParser.parseState(state);
    }
}

package ru.sstu.diplom.utils;

import ru.sstu.diplom.entities.model.Cell;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 20.11.2016.
 */
public class CellManager {

    public static List<List<Cell>> getFields(int x, int y){

        List<List<Cell>> fields = new ArrayList<List<Cell>>();
        for(int i=0; i< x; i++){
            List<Cell> column= new ArrayList<Cell>();
            for(int j=0; j<y; j++){
                column.add(new Cell(i, j));
            }
            fields.add(column);
        }
        return fields;
    }

    public static List<List<Cell>> getFields(int x, int y, double size){

        List<List<Cell>> fields = new ArrayList<List<Cell>>();
        for(int i=0; i< x; i++){
            List<Cell> column= new ArrayList<Cell>();
            for(int j=0; j<y; j++){
                Cell cell = new Cell(i, j);
                cell.resize(size);
                column.add(cell);
            }
            fields.add(column);
        }
        return fields;
    }
}

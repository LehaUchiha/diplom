package ru.sstu.diplom.utils;

import javafx.animation.FadeTransition;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.entities.model.Lesson;
import ru.sstu.diplom.entities.model.lessons.FirstLesson;
import ru.sstu.diplom.entities.model.lessons.SecondLesson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Alex on 22.05.2017.
 */
public class LessonManager {

    public static final int FIRST_LESSON = 0;
    public static final int SECOND_LESSON = 1;
    public static final int THIRD_LESSON = 2;

    private List<Lesson> lessons;

    private Lesson currentLesson;

    public LessonManager(int lesson) {
        lessons = new ArrayList<>();

        lessons.add(new FirstLesson());
        lessons.add(new SecondLesson());

        currentLesson = lessons.get(lesson);
    }

    public void setLesson(int number){
        currentLesson = lessons.get(number);
    }

    public Lesson getLesson(){
        return currentLesson;
    }

    public boolean checkAnswer(List<List<Cell>> field) {
        return currentLesson.checkAnswer(field);
    }

    public void nextTask(AnchorPane parentLesson, VBox toolsPane, Label topToolsBar, Pane centerBar, Label commentInfo) throws Exception {
        currentLesson.nextTask(toolsPane, topToolsBar, centerBar, commentInfo);

        FadeTransition fadeTransition = new FadeTransition(Duration.seconds(1.5), parentLesson);
        fadeTransition.setFromValue(0.0);
        fadeTransition.setToValue(1.0);
        fadeTransition.play();
    }
}

package ru.sstu.diplom.utils;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventTarget;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Lighting;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.sstu.diplom.constants.ApplicationOperation;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.utils.impl.DetailedInformationControllerImpl;

import javax.tools.Tool;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import static ru.sstu.diplom.constants.CellStates.*;

/**
 * Created by Alex on 03.12.2016.
 */
public class ActionHandlerManager {

    public static ApplicationOperation OPERATION = ApplicationOperation.CURSOR;

    public static TableView tableView;
    public static AdditionInformationHandler infoHandler = new AdditionInformationHandler();
    public static DetailedInformationController detailedInformationController = new DetailedInformationControllerImpl();

    public static void handleField(final List<List<Cell>> field) throws FileNotFoundException {
        for (final List<Cell> row : field) {
            for (Cell cell : row) {
                final Cell cel = cell;

                cel.setOnMouseClicked(new javafx.event.EventHandler<MouseEvent>() {
                    public void handle(MouseEvent me) {

                        switch(OPERATION){
                            case CURSOR:{
                                if(me.getButton().ordinal() == 3){
                                    detailedInformationController.getDetailedInformationAbouCell(cel);
                                }
                                resetEffectsOnField(field);
                                infoHandler.showInformationInTable(tableView, cel);
                                cel.setEffect(new Lighting());

                                break;
                            }
                            case SET_ACTIVE:{
                                if(me.getButton().ordinal() == 3)
                                    cel.setActive(false);
                                if(me.getButton().ordinal() == 1)
                                    cel.setActive(true);
                                break;
                            }
                            case T00:{
                                addAction(me, cel, T000);
                                break;
                            }
                            case T01:{
                                addAction(me, cel, T010);
                                break;
                            }
                            case T02:{
                                addAction(me, cel, T020);
                                break;
                            }
                            case T03:{
                                addAction(me, cel, T030);
                                break;
                            }
                            case T10:{
                                addAction(me, cel, T100);
                                break;
                            }
                            case T11:{
                                addAction(me, cel, T110);
                                break;
                            }
                            case T12:{
                                addAction(me, cel, T120);
                                break;
                            }
                            case T13:{
                                addAction(me, cel, T130);
                                break;
                            }
                            case SO:{
                                addAction(me, cel, SO);
                                break;
                            }
                            case S0:{
                                addAction(me, cel, S0);
                                break;
                            }
                            case S1:{
                                addAction(me, cel, S1);
                                break;
                            }
                            case S00:{
                                addAction(me, cel, S00);
                                break;
                            }
                            case S01:{
                                addAction(me, cel, S01);
                                break;
                            }
                            case S10:{
                                addAction(me, cel, S10);
                                break;
                            }
                            case S11:{
                                addAction(me, cel, S11);
                                break;
                            }
                            case S000:{
                                addAction(me, cel, S000);
                                break;
                            }
                            case C:{
                                addAction(me, cel, C00);
                                break;
                            }
                            case U:{
                                addAction(me, cel, U);
                                break;
                            }
                        }

                        try {
                            GrapchicManager.update(field);
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        me.consume();
                    }
                });

            }
        }
    }

    private static void addAction(MouseEvent me, Cell cel, CellStates cellState){
        if(me.getButton().ordinal() == 1) {
            CellStates state = cellState;
            cel.setState(state);
        }
    }

    public static void resetEffectsOnField(final List<List<Cell>> field){
        for(List<Cell> column: field)
            for(Cell cell: column){
                cell.setEffect(null);
            }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    GrapchicManager.update(field);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }});
    }

    public static void clearField(final List<List<Cell>> field){
        for(List<Cell> column: field)
            for(Cell cell: column){
                cell.setState(U);
            }

        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                try {
                    GrapchicManager.update(field);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }});

    }

}

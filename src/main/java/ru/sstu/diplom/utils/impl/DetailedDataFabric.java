package ru.sstu.diplom.utils.impl;

import ru.sstu.diplom.utils.detailedData.DetailedDataProcessor;
import ru.sstu.diplom.utils.detailedData.impl.BaseDetailedDataImpl;
import ru.sstu.diplom.utils.detailedData.impl.SensitiveDetailedDataImpl;
import ru.sstu.diplom.utils.detailedData.impl.TransitiveDetailedDataImpl;

/**
 * Created by Alex on 20.05.2017.
 */
public class DetailedDataFabric {

    public DetailedDataProcessor getDataAbout(String state){
        try {
            switch (state.substring(0, 1)){
                case "T":
                case "C": return new TransitiveDetailedDataImpl(state);
                case "S": return new SensitiveDetailedDataImpl(state);
                case "U": return new BaseDetailedDataImpl(state);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}

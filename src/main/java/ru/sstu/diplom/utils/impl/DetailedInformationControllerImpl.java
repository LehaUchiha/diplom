package ru.sstu.diplom.utils.impl;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.utils.DetailedInformationController;
import ru.sstu.diplom.utils.DetailedInformationProcessor;

import java.io.IOException;

/**
 * Created by Alex on 06.05.2017.
 */
public class DetailedInformationControllerImpl implements DetailedInformationController {

    private DetailedInformationProcessor detailedInformationProcessor;
    private DetailedDataFabric detailedDataFabric;

    public DetailedInformationControllerImpl() {

        detailedInformationProcessor = new DetailedInformationProcessor();

    }

    @Override
    public void getDetailedInformationAbouCell(Cell cell) {

        switch (cell.getState()){
            case U: detailedInformationProcessor.process(ViewConstants.DU, cell.getState().name());
                break;
            case SO:
            case S0:
            case S1:
            case S00:
            case S01:
            case S10:
            case S11:
            case S000: detailedInformationProcessor.process(ViewConstants.DSQ, cell.getState().name());
                break;
            default: detailedInformationProcessor.process(ViewConstants.DT000, cell.getState().name());
                break;
        }


    }
}

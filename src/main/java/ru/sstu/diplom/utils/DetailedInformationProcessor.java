package ru.sstu.diplom.utils;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.controllers.*;
import ru.sstu.diplom.controllers.DetailedInformationController;

import java.io.IOException;

/**
 * Created by Alex on 06.05.2017.
 */
public class DetailedInformationProcessor {

    public void process(String view, String state){
        Stage dialog = new Stage();
        dialog.setTitle("Подробности");
        dialog.setResizable(false);

        FXMLLoader loader = new FXMLLoader();
        loader.setController(new DetailedInformationController(state));
        try {
            Parent root = loader.load(getClass().getResourceAsStream(view));

            Scene sc = new Scene(root);

            sc.getStylesheets().add("/css/style.css");
            dialog.setScene(sc);
            dialog.initModality(Modality.APPLICATION_MODAL);
            dialog.showAndWait();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

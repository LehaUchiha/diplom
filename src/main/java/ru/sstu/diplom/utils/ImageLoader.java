package ru.sstu.diplom.utils;

import javafx.scene.image.Image;

import javafx.scene.image.ImageView;

import java.io.FileNotFoundException;

/**
 * Created by Alex on 10.02.2017.
 */
public class ImageLoader {

    static ImageLoader imageLoader = new ImageLoader();

    public static final String STEP_ICO_PATH = "/img/matrix_symb.jpg";
    public static final String LEFT_ARROW_ICO_PATH = "/img/arrowleft.png";
    public static final String RIGHT_ARROW_ICO_PATH = "/img/arrowright.png";

    public static final String U_IMG = "/img/cell.png";
    public static final String T030_IMG = "/img/T030.png";
    public static final String T031_IMG = "/img/T031.png";
    public static final String T000_IMG = "/img/T000.png";
    public static final String T001_IMG = "/img/T001.png";
    public static final String T010_IMG = "/img/T010.png";
    public static final String T011_IMG = "/img/T011.png";
    public static final String T020_IMG = "/img/T020.png";
    public static final String T021_IMG = "/img/T021.png";
    public static final String T100_IMG = "/img/T100.png";
    public static final String T101_IMG = "/img/T101.png";
    public static final String T110_IMG = "/img/T110.png";
    public static final String T111_IMG = "/img/T111.png";
    public static final String T121_IMG = "/img/T121.png";
    public static final String T130_IMG = "/img/T130.png";
    public static final String T131_IMG = "/img/T131.png";
    public static final String T120_IMG = "/img/T120.png";
    public static final String C00_IMG = "/img/C00.png";
    public static final String C01_IMG = "/img/C01.png";
    public static final String C10_IMG = "/img/C10.png";
    public static final String C11_IMG = "/img/C11.png";
    public static final String S0_IMG = "/img/S0.png";
    public static final String S00_IMG = "/img/S00.png";
    public static final String S000_IMG = "/img/S000.png";
    public static final String S1_IMG = "/img/S1.png";
    public static final String S01_IMG = "/img/S01.png";
    public static final String S10_IMG = "/img/S10.png";
    public static final String S11_IMG = "/img/S11.png";
    public static final String SO_IMG = "/img/SO.png";

    public static ImageView getImageView(String path) throws FileNotFoundException {
        Image image = getImage(path);
        return new ImageView(image);
    }

    public static Image getImage(String path) throws FileNotFoundException {
        return new Image(imageLoader.getClass().getResourceAsStream(path));
    }

}

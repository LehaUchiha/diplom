package ru.sstu.diplom.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.*;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.controllers.AbstractController;
import ru.sstu.diplom.controllers.ApplicationController;
import ru.sstu.diplom.entities.Statistic;
import ru.sstu.diplom.entities.model.Cell;

import javax.swing.*;
import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by Alex on 02.12.2016.
 */
public class GrapchicManager {

    public static AbstractController applicationController;

    public static void drawCells(List<List<Cell>> field) throws FileNotFoundException {

//        for (List<Cell> column : field) {
//            for (Cell cell : column) {
//                cell.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.REPEAT,
//                        new Stop[]{
//                                new Stop(0, Color.web("#4977A3")),
//                                new Stop(0.5, Color.web("#B0C6DA")),
//                                new Stop(1, Color.web("#9CB6CF")),}));
//                cell.setStroke(Color.web("#D0E6FA"));
//                // cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.STEP_ICO_PATH)));
//
//            }
//        }
    }

    public static void update(final List<List<Cell>> field) throws FileNotFoundException {
        for (List<Cell> column : field) {
            for (Cell cell : column) {

                switch(cell.getState()){
                    case T000:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T000_IMG)));
                        break;
                    }
                    case T010:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T010_IMG)));
                        break;
                    }
                    case T020:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T020_IMG)));
                        break;
                    }
                    case T030:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T030_IMG)));
                        break;
                    }
                    case T100:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T100_IMG)));
                        break;
                    }
                    case T110:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T110_IMG)));
                        break;
                    }
                    case T120:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T120_IMG)));
                        break;
                    }
                    case T130:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T130_IMG)));
                        break;
                    }
                    case T001:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T001_IMG)));
                        break;
                    }
                    case T011:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T011_IMG)));
                        break;
                    }
                    case T021:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T021_IMG)));
                        break;
                    }
                    case T031:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T031_IMG)));
                        break;
                    }
                    case T101:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T101_IMG)));
                        break;
                    }
                    case T111:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T111_IMG)));
                        break;
                    }
                    case T121:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T121_IMG)));
                        break;
                    }
                    case T131:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.T131_IMG)));
                        break;
                    }
                    case SO:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.SO_IMG)));
                        break;
                    }
                    case S0:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S0_IMG)));
                        break;
                    }
                    case S1:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S1_IMG)));
                        break;
                    }
                    case S00:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S00_IMG)));
                        break;
                    }
                    case S01:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S01_IMG)));
                        break;
                    }
                    case S10:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S10_IMG)));
                        break;
                    }case S11:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S11_IMG)));
                        break;
                    }case S000:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.S000_IMG)));
                        break;
                    }
                    case C00:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.C00_IMG)));
                        break;
                    }
                    case C01:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.C01_IMG)));
                        break;
                    }
                    case C10:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.C10_IMG)));
                        break;
                    }
                    case C11:{
                        cell.setFill(new ImagePattern(ImageLoader.getImage(ImageLoader.C11_IMG)));
                        break;
                    }
                    case U:{
                        cell.setFill(new LinearGradient(0, 0, 0, 1, true, CycleMethod.REPEAT,
                                new Stop[]{
                                        new Stop(0, Color.web("#4977A3")),
                                        new Stop(0.5, Color.web("#B0C6DA")),
                                        new Stop(1, Color.web("#9CB6CF")),}));
                        cell.setStroke(Color.web("#D0E6FA"));
                        break;
                    }
                }
            }
        }
        applicationController.setField(field);
        applicationController.updateField(field);
    }
}

package ru.sstu.diplom.utils;

import ru.sstu.diplom.entities.model.Cell;

/**
 * Created by Alex on 06.05.2017.
 */
public interface DetailedInformationController {

    void getDetailedInformationAbouCell(Cell cell);
}

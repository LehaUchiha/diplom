package ru.sstu.diplom.parser;

import ru.sstu.diplom.WrongConfigurationException;
import ru.sstu.diplom.entities.model.Cell;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Alex on 29.04.2017.
 */
public interface Parser {
    List<List<Cell>> getField(InputStream inputStream) throws Exception;
    void saveObject(File file, List<List<Cell>> field);


}

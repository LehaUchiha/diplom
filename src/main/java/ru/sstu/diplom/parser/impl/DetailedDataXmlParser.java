package ru.sstu.diplom.parser.impl;

import javafx.scene.control.Label;
import javafx.scene.text.Text;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import java.io.File;
import java.io.IOException;

/**
 * Created by Alex on 20.05.2017.
 */
public class DetailedDataXmlParser {

   public Element parseState(String state) throws JDOMException, IOException {
       SAXBuilder saxBuilder = new SAXBuilder();

       Document document = saxBuilder.build(getClass().getResourceAsStream("/db/database.xml"));
       Element rootNode = document.getRootElement();
       Element elementT = rootNode.getChild(state.substring(0,1));
       Element element = (Element) elementT.getChild(state);

       return element;
   }

}

package ru.sstu.diplom.parser.impl;

import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import ru.sstu.diplom.WrongConfigurationException;
import ru.sstu.diplom.constants.CellStates;
import ru.sstu.diplom.entities.model.Cell;
import ru.sstu.diplom.parser.Parser;

import javax.xml.bind.JAXBException;
import java.io.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;


/**
 * Created by Alex on 29.04.2017.
 */
public class JDomParser implements Parser{

    @Override
    public List<List<Cell>> getField(InputStream inputStream) throws Exception {
        List<List<Cell>> field = new ArrayList<>();

        SAXBuilder saxBuilder = new SAXBuilder();

        try {
            Document document = saxBuilder.build(inputStream);
            Element rootNode = document.getRootElement();
            List<Element> rowList = rootNode.getChildren("row");
            if(rowList == null || rowList.isEmpty())
                throw new WrongConfigurationException("Неверная конфигурация xml файла");
            for (Element rowElement : rowList) {
                List<Element> cellList = rowElement.getChildren("cell");
                List<Cell> row = new ArrayList<>();
                for(Element cell: cellList){
                    Cell cel = new Cell(Integer.parseInt(cell.getChildText("coordX")),
                            Integer.parseInt(cell.getChildText("coordY")));
                    String stateStr = cell.getAttributeValue("state");
                    cel.setState(CellStates.valueOf(stateStr));
                    row.add(cel);
                }
                field.add(row);
            }
        } catch (JDOMException | IOException ex) {
            throw new Exception(ex.getMessage(), ex);
//            Logger.getLogger(ReadXMLFileJDOMExample.class.getName())
//                    .log(Level.SEVERE, null, ex);
        }
        return field;
    }

    @Override
    public void saveObject(File file, List<List<Cell>> field) {
        Element xmlField = new Element("field");
        Document document = new Document(xmlField);

        for(List<Cell> column: field){
            Element xmlColumn = new Element("row");
            for(Cell cell: column){
                Element xmlCell = new Element("cell");
                xmlCell.setAttribute("state", cell.getState().name());
                xmlCell.addContent(new Element("coordX").setText(""+cell.getCoordX()));
                xmlCell.addContent(new Element("coordY").setText(""+cell.getCoordY()));
                xmlColumn.addContent(xmlCell);
            }
            xmlField.addContent(xmlColumn);
        }

        // Сохраняем документ в файл
        XMLOutputter xmlOutputter = new XMLOutputter();
        xmlOutputter.setFormat(Format.getPrettyFormat());
        try {
            xmlOutputter.output(document, new FileOutputStream(file));
        } catch (IOException ex) {
//            Logger.getLogger(CreateXMLFileJDOMExample.class.getName())
//                    .log(Level.SEVERE, null, ex);
        }
    }
}

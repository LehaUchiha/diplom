package ru.sstu.diplom;

/**
 * Created by Alex on 20.05.2017.
 */
public class WrongConfigurationException extends Exception {
    public WrongConfigurationException(String message) {
        super(message);
    }
}

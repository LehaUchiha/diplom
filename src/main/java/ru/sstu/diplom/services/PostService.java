package ru.sstu.diplom.services;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import ru.sstu.diplom.entities.db.Posts;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alex on 06.02.2017.
 */
public class PostService {
    private final String url = "jdbc:sqlite:main.sqlite";
    private ConnectionSource source;
    private Dao<Posts, String> dao;

    public PostService() throws SQLException {
        source = new JdbcConnectionSource(url);
        dao = DaoManager.createDao(source, Posts.class);
    }

    public List<Posts> getAll() throws SQLException {
        return dao.queryForAll();
    }
}

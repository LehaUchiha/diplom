package ru.sstu.diplom.services;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import ru.sstu.diplom.entities.db.Lessons;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alex on 06.02.2017.
 */
public class LessonService {
    private final String url = "jdbc:sqlite:main.sqlite";
    private ConnectionSource source;
    private Dao<Lessons, String> dao;

    public LessonService() throws SQLException {
        source = new JdbcConnectionSource(url);
        dao = DaoManager.createDao(source, Lessons.class);
    }

    public List<Lessons> getAll() throws SQLException {
        return dao.queryForAll();
    }
}

package ru.sstu.diplom.services;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import ru.sstu.diplom.entities.db.Step;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Alex on 06.02.2017.
 */
public class StepService {
    private final String url = "jdbc:sqlite:main.sqlite";
    private ConnectionSource source;
    private Dao<Step, String> dao;

    public StepService() throws SQLException {
        source = new JdbcConnectionSource(url);
        dao = DaoManager.createDao(source, Step.class);
    }

    public List<Step> getStepsByLessonId(int lesson_id) throws SQLException {
        return dao.queryForEq("lesson_id_fk", lesson_id);
    }
}

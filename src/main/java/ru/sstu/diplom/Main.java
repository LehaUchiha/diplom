package ru.sstu.diplom;


import com.sun.javafx.application.LauncherImpl;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import ru.sstu.diplom.constants.ViewConstants;
import ru.sstu.diplom.controllers.ApplicationController;
import ru.sstu.diplom.controllers.LessonController;
import ru.sstu.diplom.controllers.MenuController;

/**
 * Created by Alex on 16.10.2016.
 */
public class Main extends Application {


    public static void main(String[] args) {

        LauncherImpl.launchApplication(Main.class, PreloaderWindow.class, args);
    }

    public void start(Stage stage) throws Exception {

        setStagForControllers(stage);
        FXMLLoader loader = new FXMLLoader();
        loader.setController(new ApplicationController());
        Parent root = loader.load(getClass().getResourceAsStream(ViewConstants.APPLICATION_FXML));

        Scene scene = new Scene(root);

        acceptStyle(scene);
        ApplicationController controller = loader.getController();
        controller.addScreenSizeListener(controller.getFieldContainer());

        stage.setScene(scene);
        stage.setTitle("��������� ���������� �������� ��� �������");
        stage.setMinWidth(790);
        stage.setMinHeight(685);
        stage.setMaxWidth(1350);
        stage.show();
    }

    private void setStagForControllers(Stage stage){

        ApplicationController.STAGE = stage;
        MenuController.STAGE = stage;
        LessonController.STAGE = stage;
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        System.exit(0);
    }

    public static void acceptStyle(Scene scene){
        scene.getStylesheets().add("/css/style.css");
    }
}
